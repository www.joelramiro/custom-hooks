import { useState } from "react";

export const useCounter = (initialValue = 10) => {

    const [Counter, setCounter] = useState(initialValue)
    
    const increment = (val = 1)=>{
        setCounter(Counter+val)
    }
    
    const decrement = (val = 1)=>{
        if(Counter===1) return;
        setCounter(Counter-val);
    }

    const reset = () =>{
        setCounter(initialValue);
    }

    return {
        counter:Counter,
        increment,
        decrement,
        reset,
    };
}
