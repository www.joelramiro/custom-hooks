import { useEffect, useState } from "react";

export const useFetch = (url) => {
  
    const [State, setState] = useState({
        data:null,
        isLoading:true,
        hasHerror: null,
    })

    const getFetch =async()=>{
        setState({
            ...State,
            isLoading:true,
        });

            const result = await fetch(url);
            const data = await result.json();
            setState({
                data:data,
                isLoading:false,
                hasHerror:null,
            });
        };

    useEffect(() => {
      getFetch();
    }, [url])
    
  
    return {
        data: State.data,
        isLoading:State.isLoading,
        hasHerror:State.hasHerror,
    };
}
