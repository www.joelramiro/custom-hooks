import { useState } from "react";

export const useForm = (initialForm={}) => {
  
    const [formState, setformState] = useState(initialForm);

    /**{
        username:'',
        userEmail:'',
        userPassword:''
        const {username,userEmail,userPassword} = formState;
    } */
    
    const onInputChange=({target})=>{
        const {name,value} = target;
        setformState({
            ...formState,
            [name] : value, formState
        });
    }

    const onResetForm=()=>{
        setformState(initialForm);
    }


    return{
        ...formState,
        formState,
        onInputChange,
        onResetForm,
    };
}
