import { useEffect, useReducer } from "react"
import { todoReducer } from "./todoReducer";

const init = ()=>{
    return JSON.parse(localStorage.getItem('todos')) || [];
}

export const useToDo = () => {
    
const valDef = init;
const [todos, dispatchTodoAction] = useReducer(todoReducer, valDef,valDef);

useEffect(() => {
  localStorage.setItem('todos',JSON.stringify(todos||[]));
}, [todos])


const handleSubmit = (todo) => {
  const action = 
  {
    type:'[TODO] Add Todo',
    payload:todo
  };
  
  dispatchTodoAction(action);
};

const handleRemoveTodo = (todoId) => {
  const action = 
  {
    type:'[TODO] Remove Todo',
    payload:todoId
  };
  
  dispatchTodoAction(action);
};

const onToggleToDo = (todoId) => {
  const action = 
  {
    type:'[TODO] Toggle Todo',
    payload:todoId
  };
  
  dispatchTodoAction(action);
};

    return{
        todos,
        handleSubmit,
        handleRemoveTodo,
        onToggleToDo,
        allTodosCount : todos.length,
        pendingTodosCount : todos.filter(t => !t.done).length,
    };
}
